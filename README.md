# db_AI-4

This repository contains the code to the case study Ismail and Andreas had to
make during the 4th semester.
The repository exists mostly as a place to work together and for educational
purposes in case someone needs inspiration for this own school projects.

Dieses Repository enthält den Code der Datenbank Case Study von Ismail und
Andreas. Das Repository existiert hauptsächlich für die Zusammenarbeit sowie als
Inspiration falls jemand ähnlich Schulprojekte hat.

### Installation und Guidelines

##### Dokumentation

Die Inhalte der Dokumentation werden in die content.tex Datei geschrieben.
Die restlichen Dateien sind als statische Files gedacht welche möglichst nicht
mehr verändert werden sollten. Ihre Funktionen sind dabei:

- main.tex: Dient als Hauptfile zum kompilieren
- style.tex:  Enthält alle Einstellungen zum Design und weitere Packete
- titlepage.tex:  Enthält nur den Inhalt des Titelbildes
- bib.bib: Enthält die Quellen


##### Installation

1. Das SQL Studio Projekt im Ordner "sql" öffnen. Anschliessend die SQL Scripts
   entsprechend ihrer Nummerierung ausführen:

```
 * setup_01_create_database.sql
 * setup_02_create_table.sql
 * setup_03_insert_data.sql
 * setup_04_create_views.sql
```

2. Das Visual Studio Projekt im Ordner
   "csharp" öffnen.

3. Den Connection String in den Properties der Datei App.config anpassen.

4. Applikation kompilieren.

### Support

We don't provide any support for the content in this repository.

### License

The project is licensed under the GPLv3 license.
