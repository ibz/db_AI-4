-- Removes the tables for the market DB
--
-- Author: Andreas Zweili
-- Erstellt: 2017-05-27
-- DB-Server SQL Server 2016

use master;
if exists (select * from sys.databases where name='marketdb')
    drop DATABASE marketdb;
