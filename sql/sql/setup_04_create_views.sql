-- Creates views in the marketdb database
--
-- Author: Andreas Zweili
-- Erstellt: 2017-08-08
-- DB-Server SQL Server 2016

-- create RentedLocations view
use marketdb;
go

	CREATE VIEW RentedLocations AS
		SELECT dbo.locations.location_name,
			dbo.locations.location_capacity,
			dbo.rent_prices.rent_price,
			dbo.rents.rent_date,
			dbo.members.email_address
		FROM dbo.members
			INNER JOIN	dbo.rents ON dbo.members.member_id = dbo.rents.member_id
			INNER JOIN dbo.rent_prices ON dbo.rents.rent_price_id = dbo.rent_prices.rent_price_id
			INNER JOIN dbo.locations ON dbo.rents.location_id = dbo.locations.location_id
