-- Dateiname create_database.sql
--
-- Beschreibung: Query zum erstellen der Case Study DB
--
-- Autor: Andreas Zweili
-- Datum: 2017-06-05
-- Server Version: SQL Server 2016

if not exists (select * from sys.databases where name='marketdb')
	CREATE DATABASE marketdb;
