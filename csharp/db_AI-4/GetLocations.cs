﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db_AI_4
{
    public class GetLocations
    {
        public int location_id { get; set; }
        public string streetname { get; set; }
        public int location_capacity { get; set; }
        public string location_name { get; set; }
        public int rent_price_id { get; set; }


        public string LocationInfo
        {
            get
            {
                return $"{ streetname } { location_capacity } {location_name} ";
            }

        }
    }
}