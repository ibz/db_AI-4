﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db_AI_4
{
    public class GetRents
    {
        public int rent_id { get; set; }
        public string rent_date { get; set; }
        public string payment_date { get; set; }
        public int member_id { get; set; }
        public string email_address { get; set; }
        public int rent_price_id { get; set; }
        public int rent_price { get; set; }

        public int location_id { get; set; }
        public string streetname { get; set; }
        public int location_capacity { get; set; }
        public string location_name { get; set; }



        // Not in use until we want to read that data out
        public string RentInfo
        {
            get
            {
                return $"{location_name} {location_capacity} {rent_price} { rent_date } {email_address}  ";
            }

        }
    }
}
