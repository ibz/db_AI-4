﻿namespace db_AI_4
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LocationListBox = new System.Windows.Forms.ListBox();
            this.LocationTextBox = new System.Windows.Forms.TextBox();
            this.LocationLabel = new System.Windows.Forms.Label();
            this.LocationSearchButton = new System.Windows.Forms.Button();
            this.RentDateLabel = new System.Windows.Forms.Label();
            this.PaymentDateLabel = new System.Windows.Forms.Label();
            this.RentInsertButton = new System.Windows.Forms.Button();
            this.RentDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.UserDisplayLabel = new System.Windows.Forms.Label();
            this.ShowRentListBox = new System.Windows.Forms.ListBox();
            this.ShowRentLabel = new System.Windows.Forms.Label();
            this.ShowRentButton = new System.Windows.Forms.Button();
            this.RentedTextBox = new System.Windows.Forms.TextBox();
            this.LoggedInUserLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LocationListBox
            // 
            this.LocationListBox.FormattingEnabled = true;
            this.LocationListBox.Location = new System.Drawing.Point(9, 118);
            this.LocationListBox.Margin = new System.Windows.Forms.Padding(2);
            this.LocationListBox.Name = "LocationListBox";
            this.LocationListBox.Size = new System.Drawing.Size(362, 199);
            this.LocationListBox.TabIndex = 0;
            // 
            // LocationTextBox
            // 
            this.LocationTextBox.Location = new System.Drawing.Point(9, 76);
            this.LocationTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.LocationTextBox.Name = "LocationTextBox";
            this.LocationTextBox.Size = new System.Drawing.Size(76, 20);
            this.LocationTextBox.TabIndex = 1;
            // 
            // LocationLabel
            // 
            this.LocationLabel.AutoSize = true;
            this.LocationLabel.Location = new System.Drawing.Point(9, 43);
            this.LocationLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LocationLabel.Name = "LocationLabel";
            this.LocationLabel.Size = new System.Drawing.Size(102, 13);
            this.LocationLabel.TabIndex = 2;
            this.LocationLabel.Text = "Locations-Searched";
            // 
            // LocationSearchButton
            // 
            this.LocationSearchButton.Location = new System.Drawing.Point(88, 75);
            this.LocationSearchButton.Margin = new System.Windows.Forms.Padding(2);
            this.LocationSearchButton.Name = "LocationSearchButton";
            this.LocationSearchButton.Size = new System.Drawing.Size(56, 19);
            this.LocationSearchButton.TabIndex = 3;
            this.LocationSearchButton.Text = "Search";
            this.LocationSearchButton.UseVisualStyleBackColor = true;
            this.LocationSearchButton.Click += new System.EventHandler(this.LocationSearchButton_Click);
            // 
            // RentDateLabel
            // 
            this.RentDateLabel.AutoSize = true;
            this.RentDateLabel.Location = new System.Drawing.Point(11, 330);
            this.RentDateLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.RentDateLabel.Name = "RentDateLabel";
            this.RentDateLabel.Size = new System.Drawing.Size(56, 13);
            this.RentDateLabel.TabIndex = 20;
            this.RentDateLabel.Text = "Rent-Date";
            // 
            // PaymentDateLabel
            // 
            this.PaymentDateLabel.AutoSize = true;
            this.PaymentDateLabel.Location = new System.Drawing.Point(375, 210);
            this.PaymentDateLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.PaymentDateLabel.Name = "PaymentDateLabel";
            this.PaymentDateLabel.Size = new System.Drawing.Size(0, 13);
            this.PaymentDateLabel.TabIndex = 18;
            // 
            // RentInsertButton
            // 
            this.RentInsertButton.Location = new System.Drawing.Point(295, 328);
            this.RentInsertButton.Margin = new System.Windows.Forms.Padding(2);
            this.RentInsertButton.Name = "RentInsertButton";
            this.RentInsertButton.Size = new System.Drawing.Size(76, 19);
            this.RentInsertButton.TabIndex = 17;
            this.RentInsertButton.Text = "Rent";
            this.RentInsertButton.UseVisualStyleBackColor = true;
            this.RentInsertButton.Click += new System.EventHandler(this.RentInsertButton_Click);
            // 
            // RentDateTimePicker
            // 
            this.RentDateTimePicker.Location = new System.Drawing.Point(78, 327);
            this.RentDateTimePicker.Margin = new System.Windows.Forms.Padding(2);
            this.RentDateTimePicker.Name = "RentDateTimePicker";
            this.RentDateTimePicker.Size = new System.Drawing.Size(182, 20);
            this.RentDateTimePicker.TabIndex = 21;
            // 
            // UserDisplayLabel
            // 
            this.UserDisplayLabel.AutoSize = true;
            this.UserDisplayLabel.Location = new System.Drawing.Point(9, 11);
            this.UserDisplayLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.UserDisplayLabel.Name = "UserDisplayLabel";
            this.UserDisplayLabel.Size = new System.Drawing.Size(71, 13);
            this.UserDisplayLabel.TabIndex = 27;
            this.UserDisplayLabel.Text = "Logged in as:";
            // 
            // ShowRentListBox
            // 
            this.ShowRentListBox.FormattingEnabled = true;
            this.ShowRentListBox.Location = new System.Drawing.Point(384, 118);
            this.ShowRentListBox.Margin = new System.Windows.Forms.Padding(2);
            this.ShowRentListBox.Name = "ShowRentListBox";
            this.ShowRentListBox.Size = new System.Drawing.Size(351, 199);
            this.ShowRentListBox.TabIndex = 28;
            this.ShowRentListBox.SelectedIndexChanged += new System.EventHandler(this.ShowRentListBox_SelectedIndexChanged);
            // 
            // ShowRentLabel
            // 
            this.ShowRentLabel.AutoSize = true;
            this.ShowRentLabel.Location = new System.Drawing.Point(381, 43);
            this.ShowRentLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ShowRentLabel.Name = "ShowRentLabel";
            this.ShowRentLabel.Size = new System.Drawing.Size(91, 13);
            this.ShowRentLabel.TabIndex = 29;
            this.ShowRentLabel.Text = "Rented-Locations";
            // 
            // ShowRentButton
            // 
            this.ShowRentButton.Location = new System.Drawing.Point(464, 74);
            this.ShowRentButton.Margin = new System.Windows.Forms.Padding(2);
            this.ShowRentButton.Name = "ShowRentButton";
            this.ShowRentButton.Size = new System.Drawing.Size(56, 19);
            this.ShowRentButton.TabIndex = 30;
            this.ShowRentButton.Text = "Show";
            this.ShowRentButton.UseVisualStyleBackColor = true;
            this.ShowRentButton.Click += new System.EventHandler(this.ShowRentButton_Click);
            // 
            // RentedTextBox
            // 
            this.RentedTextBox.Location = new System.Drawing.Point(384, 74);
            this.RentedTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.RentedTextBox.Name = "RentedTextBox";
            this.RentedTextBox.Size = new System.Drawing.Size(76, 20);
            this.RentedTextBox.TabIndex = 31;
            // 
            // LoggedInUserLabel
            // 
            this.LoggedInUserLabel.AutoSize = true;
            this.LoggedInUserLabel.Location = new System.Drawing.Point(84, 11);
            this.LoggedInUserLabel.Name = "LoggedInUserLabel";
            this.LoggedInUserLabel.Size = new System.Drawing.Size(100, 13);
            this.LoggedInUserLabel.TabIndex = 32;
            this.LoggedInUserLabel.Text = "LoggedInUserLabel";
            this.LoggedInUserLabel.Click += new System.EventHandler(this.LoggedInUser_Click);
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(750, 363);
            this.Controls.Add(this.LoggedInUserLabel);
            this.Controls.Add(this.RentedTextBox);
            this.Controls.Add(this.ShowRentButton);
            this.Controls.Add(this.ShowRentLabel);
            this.Controls.Add(this.ShowRentListBox);
            this.Controls.Add(this.UserDisplayLabel);
            this.Controls.Add(this.RentDateTimePicker);
            this.Controls.Add(this.RentDateLabel);
            this.Controls.Add(this.PaymentDateLabel);
            this.Controls.Add(this.RentInsertButton);
            this.Controls.Add(this.LocationSearchButton);
            this.Controls.Add(this.LocationLabel);
            this.Controls.Add(this.LocationTextBox);
            this.Controls.Add(this.LocationListBox);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Dashboard";
            this.Text = "Dashboard";
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox LocationListBox;
        private System.Windows.Forms.TextBox LocationTextBox;
        private System.Windows.Forms.Label LocationLabel;
        private System.Windows.Forms.Button LocationSearchButton;
        private System.Windows.Forms.Label RentDateLabel;
        private System.Windows.Forms.Label PaymentDateLabel;
        private System.Windows.Forms.Button RentInsertButton;
        private System.Windows.Forms.DateTimePicker RentDateTimePicker;
        private System.Windows.Forms.Label UserDisplayLabel;
        private System.Windows.Forms.ListBox ShowRentListBox;
        private System.Windows.Forms.Label ShowRentLabel;
        private System.Windows.Forms.Button ShowRentButton;
        private System.Windows.Forms.TextBox RentedTextBox;
        private System.Windows.Forms.Label LoggedInUserLabel;
    }
}