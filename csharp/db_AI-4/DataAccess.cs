﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace db_AI_4
{
    public class DataAccess
    {
        public SqlConnection connection { get; private set; }

        //Read locations from the Database function start
        public List<GetLocations> GetLocations(string LocationName)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("marketdb")))
            {

                var output = connection.Query<GetLocations>($"SELECT * FROM locations WHERE location_name like '%{ LocationName }%'").ToList();
                return output;

            }
        }

        public void CurrentUser(string input_mail)
        {
            List<int> QueryResult = new List<int>();
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("marketdb")))
            {
                var command = "SELECT member_id FROM dbo.members WHERE members.email_address = @Email_Adress";
                QueryResult = connection.Query<int>(command, new { Email_Adress = input_mail }).ToList();
                GetMembers.member_id = Convert.ToInt32(QueryResult[0]);
            }
        }


        // Insert Rents into Database start
        public void InsertRent(string RentDate,  int MemberID, int RentPriceID , int LocationID)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("marketdb")))
            {
                List<GetRents> rents = new List<GetRents>();

                rents.Add(new GetRents { rent_date = RentDate,  member_id = GetMembers.member_id, rent_price_id = RentPriceID, location_id = LocationID});

                connection.Execute("INSERT INTO dbo.rents (rent_date, member_id, rent_price_id, location_id)" +
                    " VALUES (@rent_date, @member_id, @rent_price_id, @location_id )", rents);
            }
        }
        // Insert Rents into Database end



        //Registration function start
        public void InsertMember(string input_mail, string input_pw)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("marketdb")))
            {
                var command = "INSERT INTO dbo.members (email_address, password) VALUES ( @Email_address, @Password)";
                connection.Execute(command, new { Email_address = input_mail, Password = input_pw });

            }
        }
        //Registration function end

        //Read Rented locations start
        internal List<GetRents> GetRentedLocations(string LocationName)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection(Helper.CnnVal("marketdb")))
            {

                var rentoutput = connection.Query<GetRents>( $"SELECT * FROM [RentedLocations] WHERE location_name like '%{ LocationName }%' ORDER BY rent_date ASC ").ToList();
                return rentoutput;

            }
        }

        //Read Rented locations end



        //CheckLogin function start
        public void CheckLogin(string Email_address, string Password)
        {
            SqlConnection con = new SqlConnection(Helper.CnnVal("marketdb"));

                con.Open();
                SqlCommand cmd = new SqlCommand($"SELECT * FROM dbo.members WHERE email_address = '{Email_address}' and password = '{Password}'",con);
                SqlDataReader reader;
                reader = cmd.ExecuteReader();

                int count = 0;
                while (reader.Read())
                {
                    count += 1;
                }

                if (count == 1)
                {
                    CurrentUser(GetMembers.email_address);
                    Dashboard dashboard = new Dashboard();
                    dashboard.Show();
                }
                else if (count > 0)
                {
                    MessageBox.Show("Already existing");
                }
                else
                {
                    MessageBox.Show("Username or Password not correct");

                }
            //CheckLogin function end
        }
    }
}