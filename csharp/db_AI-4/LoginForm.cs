﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace db_AI_4
{
    public partial class LoginForm : Form
    {

        public LoginForm()
        {
            InitializeComponent();

        }

        private void  RegistrationButton_Click(object sender, EventArgs e)
        {
            DataAccess db = new DataAccess();
            GetMembers.email_address = EmailLoginBox.Text;
            GetMembers.password = PasswordLoginBox.Text;
            db.InsertMember(GetMembers.email_address, GetMembers.password);
            MessageBox.Show("Member Registered.");
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            DataAccess db = new DataAccess();

            GetMembers.email_address = EmailLoginBox.Text;
            GetMembers.password = PasswordLoginBox.Text;
            db.CheckLogin(GetMembers.email_address, GetMembers.password);

        }

        private void LoginForm_Load(object sender, EventArgs e)
        {

        }
    }

}
