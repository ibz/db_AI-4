﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace db_AI_4
{
    public static class GetMembers
    {
        public static int member_id { get; set; }
        public static string email_address { get; set; }
        public static string password { get; set; }
        public static int member_status_id { get; set; }


        public static string MemberInfo
        {
            get
         {
             return $" {member_id} ({email_address}) {password} ";
         }

        }

        public static string MemberID
        {
            get
            {
                return $"({email_address}) {member_id}";
            }
        }
    }
}

