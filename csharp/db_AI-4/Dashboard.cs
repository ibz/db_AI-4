﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace db_AI_4
{
    public partial class Dashboard : Form
    {
        //Search Locations start
        List<GetLocations> locations = new List<GetLocations>();
        List<GetLocations> rentlocation = new List<GetLocations>();
        List<GetRents> rentedlocations = new List<GetRents>();

        BindingSource locationBinding = new BindingSource();
        BindingSource addlocationBinding = new BindingSource();
        BindingSource showrentBinding = new BindingSource();

        public Dashboard()
        {
            InitializeComponent();
            UpdateBinding();
            LoggedInUserLabel.Text = GetMembers.email_address;
        }

        private void UpdateBinding()
        {
            // Loading of the Searched Locations
            LocationListBox.DataSource = locations;
            LocationListBox.DisplayMember = "LocationInfo";

            locationBinding.DataSource = locations;
            LocationListBox.DataSource = locationBinding;


            //Loading of the Selected  Locations

            // Rented Locations
            ShowRentListBox.DataSource = rentedlocations;
            ShowRentListBox.DisplayMember = "RentInfo";

            showrentBinding.DataSource = rentedlocations;
            ShowRentListBox.DataSource = showrentBinding;

        }



        private void LocationSearchButton_Click(object sender, EventArgs e)
        {
            DataAccess db = new DataAccess();

            locations = db.GetLocations(LocationTextBox.Text);

            UpdateBinding();

        }
        // Search Locations end


        // Insert Rent start
        private void RentInsertButton_Click(object sender, EventArgs e)
        {
            DataAccess db = new DataAccess();
            try
            {
                GetLocations selectedItem = (GetLocations)LocationListBox.SelectedItem;
                //var command = "SELECT rent_price_id FROM dbo.locations WHERE locations.location_name = @Location_Name";
                selectedItem = db.GetLocations(selectedItem.location_name)[0];
                db.InsertRent(RentDateTimePicker.Value.Date.ToString("yyyyMMdd"), GetMembers.member_id, selectedItem.rent_price_id, selectedItem.location_id);
                //RentDateTimePicker.Text = "";
            }
            catch
            {
                    MessageBox.Show("Please select a location and a date.");
            }
            
        }
        // Insert Rent end


        private void Dashboard_Load(object sender, EventArgs e)
        {

        }

        private void ShowRentButton_Click(object sender, EventArgs e)
        {
            DataAccess db = new DataAccess();

            rentedlocations = db.GetRentedLocations(RentedTextBox.Text);

            UpdateBinding();
        }

        private void ShowRentListBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void LoggedInUser_Click(object sender, EventArgs e)
        {

        }

        //Add Location to ADDBox end

    }
}
